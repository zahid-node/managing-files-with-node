const { createReadStream } = require('fs');

const stream = createReadStream('./app.log',
    {
        highWaterMark:9950,
        encoding:'utf-8'
    
    });

stream.on('data',data => console.log(data));