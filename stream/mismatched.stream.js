const { createReadStream,createWriteStream } = require('fs');

const stream = createReadStream('./stream.log',
    {
        encoding:'utf-8'
    
    }
);

const writer = createWriteStream('./output.log');

// let iteration = 0;
// stream.on('data',data =>{
//     console.log(++iteration);
//     writeData(data);
// });

// const writeData = (data) => {
//     setTimeout(() => {
//        writer.write(data);
//     },6000);
// }

// To prevent the stream mismatch we can use pipe method

stream.pipe(writer);