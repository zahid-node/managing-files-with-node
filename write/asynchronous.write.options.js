// const {writeFile} = require('fs');

// writeFile('./data/app.log',
//     '163.3.217.18 - - [21/09/2019:10:07:21 -0500] "GET /write-dot-com" 405 21512',
//     {flag:'wx'},
//     (err) => {
//         err ? console.log(err) : console.log("file saved!");
//     }
// );

// write a file which has permission to owner only

// const {constants,writeFile} = require('fs');

// writeFile('./data/newapp.log',
//     '163.3.217.18 - - [21/09/2019:10:07:21 -0500] "GET /write-dot-com" 405 21512',
//     {mode:constants.S_IWUSR | constants.S_IRUSR},
//     (err) => {
//         err ? console.log(err) : console.log("file saved!");
//     }
// );

// create a base 64 file
const {writeFile} = require('fs');

writeFile('./data/newapp.log',
    '163.3.217.18 - - [21/09/2019:10:07:21 -0500] "GET /write-dot-com" 405 21512',
    {encoding:'base64'},
    (err) => {
        err ? console.log(err) : console.log("file saved!");
    }
);