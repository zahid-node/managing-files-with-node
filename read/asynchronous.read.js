const {convertCsv} = require('./csv.parse');
const {readFile} = require('fs');

modules.exports.read = () => {
    readFile('./data/pulitzer-circulation-data.csv','utf-8',(err,data) => {
        if(err){
            console.log(`There was a problem with the files ${err}`);
            return
        }
        const vals = convertCsv(data);
        console.table(vals);

    }); 
}