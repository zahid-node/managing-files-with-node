const {closeSync,openSync,readdirSync,writeSync,watch} = require('fs');
const camelCase = require('camelcase');

watch('./read',() => {
    const indexFd = openSync('./index.js','w');

    const files = readdirSync('./read');
    files.map(file => {
        const name = file.replace('js','');
        console.log(`Adding a file: ${file}`);
        writeSync(indexFd,
            `module.exports.${camelCase(name)} = require('./read/${name}').read;\n`);

    });

    closeSync(indexFd);

});
