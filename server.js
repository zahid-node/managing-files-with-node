const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


app.get('/',(req,res)=>{
    res.send('Hello from managing files with node');
});

app.listen(port,() => {
    console.log(`Server is up and running on port : ${port}`);
});